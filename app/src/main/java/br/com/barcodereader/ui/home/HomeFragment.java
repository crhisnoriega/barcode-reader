package br.com.barcodereader.ui.home;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.room.Room;

import com.google.android.material.textfield.TextInputEditText;

import br.com.barcodereader.R;
import br.com.barcodereader.persistence.Database;
import br.com.barcodereader.persistence.Product;


public class HomeFragment extends Fragment {

    private TextInputEditText codebar;
    private TextInputEditText qtde;

    private TextView codebarlast;
    private TextView qtdelast;
    private TextView coletor;
    private Switch enableQtde;
    private ImageView edit;

    private Database database;

    private CardView lastScan;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        this.database = Room.databaseBuilder(getActivity(), Database.class, "products").build();

        codebar = root.findViewById(R.id.codebar);
        codebarlast = root.findViewById(R.id.codebarlast);
        qtde = root.findViewById(R.id.qtde);
        qtdelast = root.findViewById(R.id.qtdelast);
        enableQtde = root.findViewById(R.id.enableQtde);
        edit = root.findViewById(R.id.edit);
        coletor = root.findViewById(R.id.coletor);

        lastScan = root.findViewById(R.id.lastscan);


        codebar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE) || ((event.getKeyCode() == KeyEvent.KEYCODE_ENTER) && (event.getAction() == KeyEvent.ACTION_DOWN))) {
                    qtde.requestFocus();
                    return true;
                } else {
                    return false;
                }
            }
        });

        enableQtde.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                qtde.setEnabled(enableQtde.isChecked());
                edit.setVisibility(enableQtde.isChecked() ? View.VISIBLE : View.GONE);

                if (enableQtde.isChecked()) {
                    qtde.requestFocus();
                    qtde.setSelection(qtde.getText().length());
                    showKeyboard();
                }
            }
        });



        qtde.setEnabled(enableQtde.isChecked());
        qtde.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE) || ((event.getKeyCode() == KeyEvent.KEYCODE_ENTER) && (event.getAction() == KeyEvent.ACTION_DOWN))) {
                    updateFromEdit();
                    return true;
                } else {
                    return false;
                }
            }
        });

        qtde.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b){
                    qtde.clearFocus();
                    hideKeyboard(getActivity());
                    enableQtde.setChecked(false);
                }
            }
        });

        edit.setVisibility(enableQtde.isChecked() ? View.VISIBLE : View.GONE);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateFromEdit();
            }
        });

        this.updateLastProduct();


        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("coletor", Context.MODE_PRIVATE);
        String coletor = sharedPreferences.getString("coletor","Coletor 1");
        this.coletor.setText(coletor);


        return root;
    }

    private void updateFromEdit() {
        if (codebar.getText().toString().isEmpty()) {
            new AlertDialog.Builder(getActivity()).setMessage("Codigo de barras inválido").setPositiveButton("Confirmar", new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).setCancelable(false).show();
            return;
        }

        try {
            updateCodebar(codebar.getText().toString(), Integer.parseInt(qtde.getText().toString()));
            qtde.clearFocus();
            hideKeyboard(getActivity());
            enableQtde.setChecked(false);
        } catch (Exception e) {

        }
    }

    // update 1
    public void updateCodebar(final String codebar) {
        this.codebar.setText(codebar);

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            Product product;

            @Override
            protected Void doInBackground(Void... voids) {
                this.product = database.productDao().findByCodebar(codebar);

                if (this.product == null) {
                    this.product = new Product();
                    this.product.setId(codebar);
                    this.product.setQuantidade(1);
                    database.productDao().insertAll(this.product);
                } else {
                    this.product.setQuantidade(product.getQuantidade() + 1);
                    database.productDao().update(product);
                }

                lastProduct = this.product;

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                qtde.setText(product.getQuantidade() + "");
                updateLastProduct();
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }

    private Product lastProduct;

    public void updateLastProduct() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (lastProduct != null) {
                    lastScan.setVisibility(View.VISIBLE);
                    codebarlast.setText(lastProduct.getId());
                    qtdelast.setText(lastProduct.getQuantidade() + "");
                } else {
                    lastScan.setVisibility(View.GONE);
                }
            }
        });

    }

    // update 2
    public void updateCodebar(final String codebar, final int qtdeValue) {
        this.codebar.setText(codebar);

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

            Product product;

            @Override
            protected Void doInBackground(Void... voids) {
                this.product = database.productDao().findByCodebar(codebar);

                if (this.product == null) {
                    this.product = new Product();
                    this.product.setId(codebar);
                    this.product.setQuantidade(qtdeValue);
                    database.productDao().insertAll(this.product);
                } else {
                    this.product.setQuantidade(this.product.getQuantidade() + qtdeValue);
                    database.productDao().update(product);
                }

                lastProduct = this.product;

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                qtde.setText(product.getQuantidade() + "");
                updateLastProduct();
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }
}
