package br.com.barcodereader.ui.dashboard;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

import br.com.barcodereader.R;
import br.com.barcodereader.persistence.Database;
import br.com.barcodereader.persistence.Product;

public class ListFragment extends Fragment {

    private RecyclerView recyclerView;
    private SearchView serchView;
    private Database database;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list, container, false);

        recyclerView = (RecyclerView) root.findViewById(R.id.rv_product);
        serchView = root.findViewById(R.id.search);

        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        MyAdapter mAdapter = new MyAdapter(new ArrayList<>());
        recyclerView.setAdapter(mAdapter);

        this.database = Room.databaseBuilder(getActivity(), Database.class, "products").build();

        populateProducts();

        this.serchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                populateProducts();
                return false;
            }
        });

        this.serchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {


            private void doSearch(final String query1) {
                AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                    private List<Product> allproducts;

                    @Override
                    protected Void doInBackground(Void... voids) {
                        this.allproducts = database.productDao().findAnyCodebar("%" + query1 + "%");
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        if (this.allproducts != null) {
                            MyAdapter mAdapter = new MyAdapter(this.allproducts);
                            recyclerView.setAdapter(mAdapter);
                        } else {
                            recyclerView.setAdapter(new MyAdapter(new ArrayList<>()));
                        }
                    }
                };
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }

            @Override
            public boolean onQueryTextSubmit(String query) {

                this.doSearch(query);


                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText != null && newText.length() > 3) {
                    this.doSearch(newText);
                }
                return false;
            }
        });

        return root;
    }

    public void populateProducts() {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            private List<Product> allproducts;

            @Override
            protected Void doInBackground(Void... voids) {
                this.allproducts = database.productDao().getAll();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                MyAdapter mAdapter = new MyAdapter(this.allproducts);
                recyclerView.setAdapter(mAdapter);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
        private List<Product> mDataset;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView codebar;
            public TextView qtde;
            public ImageView remove_item;
            public ImageView edit_query_item;
            public EditText qtdeedit;
            public boolean isEdit = false;

            public MyViewHolder(View v) {
                super(v);
                codebar = v.findViewById(R.id.codebar);
                qtde = v.findViewById(R.id.qtde);
                remove_item = v.findViewById(R.id.remove_item);
                edit_query_item = v.findViewById(R.id.edit_query_item);
                qtdeedit = v.findViewById(R.id.qtdeedit);
            }
        }

        public MyAdapter(List<Product> myDataset) {
            mDataset = myDataset;
        }

        @Override
        public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
            View root = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.product_item, parent, false);
            MyViewHolder vh = new MyViewHolder(root);
            return vh;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            holder.codebar.setText(mDataset.get(position).getId());
            holder.qtde.setText(mDataset.get(position).getQuantidade() + "");
            holder.remove_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity()).setMessage("Deseja apagar o produto " + mDataset.get(position).getId() + "?").setCancelable(false).setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... voids) {
                                    Product item = mDataset.get(position);
                                    database.productDao().delete(item);
                                    mDataset.remove(position);
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Void aVoid) {
                                    recyclerView.getAdapter().notifyDataSetChanged();
                                }
                            };

                            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    });
                    dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    dialog.show();


                }
            });

            holder.edit_query_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.isEdit) {
                        final String codebar = mDataset.get(position).getId();
                        final String qtde_input = holder.qtdeedit.getText().toString();
                        doOK(codebar, qtde_input, holder);
                    } else {
                        holder.qtdeedit.setVisibility(View.VISIBLE);
                        holder.qtdeedit.setText(mDataset.get(position).getQuantidade() + "");
                        holder.qtdeedit.requestFocus();
                        holder.isEdit = true;
                        holder.qtde.setVisibility(View.GONE);
                        holder.edit_query_item.setImageDrawable(getActivity().getDrawable(R.drawable.outline_check_24));
                        showKeyboard();
                    }
                }
            });

            holder.qtdeedit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if (!b && holder.isEdit) {
                        holder.qtde.setVisibility(View.VISIBLE);
                        holder.qtdeedit.setVisibility(View.GONE);
                        holder.isEdit = false;
                        holder.edit_query_item.setImageDrawable(getActivity().getDrawable(R.drawable.outline_edit_24));
                    }
                }
            });

            holder.qtdeedit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                    if ((actionId == EditorInfo.IME_ACTION_DONE) || ((event.getKeyCode() == KeyEvent.KEYCODE_ENTER) && (event.getAction() == KeyEvent.ACTION_DOWN))) {
                        final String codebar = mDataset.get(position).getId();
                        final String qtde_input = holder.qtdeedit.getText().toString();
                        doOK(codebar, qtde_input, holder);
                        return true;
                    } else {
                        return false;
                    }
                }
            });

        }

        private void doOK(final String codebar, final String qtde_input, MyViewHolder holder) {
            hideKeyboard(getActivity());

            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

                Product product;

                @Override
                protected Void doInBackground(Void... voids) {
                    this.product = database.productDao().findByCodebar(codebar);
                    this.product.setQuantidade(Integer.parseInt(qtde_input));
                    database.productDao().update(this.product);

                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    holder.qtde.setVisibility(View.VISIBLE);
                    holder.qtdeedit.setVisibility(View.GONE);
                    holder.isEdit = false;
                    holder.edit_query_item.setImageDrawable(getActivity().getDrawable(R.drawable.outline_edit_24));
                    holder.qtde.setText(qtde_input);
                }
            };
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

        @Override
        public int getItemCount() {
            return mDataset.size();
        }
    }

    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
