package br.com.barcodereader.ui.notifications;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.room.Room;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;

import br.com.barcodereader.R;
import br.com.barcodereader.ScannerActivity;
import br.com.barcodereader.email.MailSender;
import br.com.barcodereader.persistence.Database;
import br.com.barcodereader.persistence.Product;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SettingsFragment extends Fragment {

    private Database database;
    private TextInputEditText email;
    private TextInputEditText coletor;
    private ProgressBar progressBar;
    private View container;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_settings, container, false);
        this.container = root;
        this.database = Room.databaseBuilder(getActivity(), Database.class, "products").build();
        this.email = root.findViewById(R.id.email);
        this.coletor = root.findViewById(R.id.coletor);
        this.progressBar = root.findViewById(R.id.progressBar);

        Spinner spinnerScannerDevices = root.findViewById(R.id.spinnerScannerDevices);
        Button sendButton = root.findViewById(R.id.sendEmail);

        ScannerActivity act = (ScannerActivity) getActivity();

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, act.friendlyNameList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerScannerDevices.setAdapter(spinnerAdapter);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

                        private String coletor;
                        private String email;
                        private Exception e;

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            progressBar.setVisibility(View.VISIBLE);
                            sendButton.setAlpha(0.5f);
                            sendButton.setText("Enviando...");

                            this.coletor = SettingsFragment.this.coletor.getText().toString();
                            this.email = SettingsFragment.this.email.getText().toString();

                            if (container instanceof FrameLayout) {
                                ConstraintLayout layout = container.findViewById(R.id.constraintLayout);
                                for (int i = 0; i < layout.getChildCount(); i++) {
                                    View child = layout.getChildAt(i);
                                    child.setEnabled(false);
                                }
                            }
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            progressBar.setVisibility(View.GONE);
                            sendButton.setAlpha(1f);
                            sendButton.setText("Enviar Email");


                            if (container instanceof FrameLayout) {
                                ConstraintLayout layout = container.findViewById(R.id.constraintLayout);
                                for (int i = 0; i < layout.getChildCount(); i++) {
                                    View child = layout.getChildAt(i);
                                    child.setEnabled(true);
                                }
                            }

                            String message = "";
                            if (this.e == null) {
                                message = "Email enviado com sucesso!";
                            } else {
                                message = "Erro enviando email: " + this.e.getLocalizedMessage();
                            }
                            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity()).setMessage(message).setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    if (e == null) {
                                        askToClean();
                                    }
                                }
                            });
                            dialog.show();

                            hideKeyboard(getActivity());
                        }

                        @Override
                        protected Void doInBackground(Void... voids) {

                            List<Product> products = database.productDao().getAll();

                            writeToFile(products);

                            Hashtable<String, Object> jsonEmail = new Hashtable<>();
                            List<Hashtable<String, String>> jsonProducts = new ArrayList<>();
                            for (Product product : products) {
                                Hashtable<String, String> pp = new Hashtable<>();
                                pp.put("id", product.getId());
                                pp.put("quantidade", product.getQuantidade() + "");
                                jsonProducts.add(pp);
                            }
                            jsonEmail.put("produtos", jsonProducts);
                            jsonEmail.put("coletor", this.coletor);
                            jsonEmail.put("email", this.email);
                            String json = new Gson().toJson(jsonEmail);

                            Log.i("crhisn", "json:" + json);

                            try {
                                RequestBody body = RequestBody.create(json, MediaType.parse("application/json"));
                                Request request = new Request.Builder()
                                        .url("http://grxtrackingserver.ddns.net:8080/email")
                                        .post(body)
                                        .build();
                                OkHttpClient client = new OkHttpClient();
                                Response response = client.newCall(request).execute();

                                String jsonResponse = response.body().string();

                                Log.i("crhisn", jsonResponse);
                                if ("success".equals(jsonResponse)) {
                                } else {
                                    this.e = new Exception("Error no envio do email");
                                }

                            } catch (Exception e) {
                                Log.i("crhisn", e.getLocalizedMessage(), e);
                                this.e = e;
                            }


                            return null;
                        }
                    };
                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                } catch (Exception e) {
                    Log.i("crhisn", e.getLocalizedMessage(), e);
                }
            }
        });

        this.email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE) || ((event.getKeyCode() == KeyEvent.KEYCODE_ENTER) && (event.getAction() == KeyEvent.ACTION_DOWN))) {
                    saveKeyValue("email", email.getText().toString());
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity()).setMessage("Email salvo com sucesso!").setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            hideKeyboard(getActivity());
                        }
                    });
                    dialog.show();
                    return true;
                } else {
                    return false;
                }
            }
        });

        this.coletor.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE) || ((event.getKeyCode() == KeyEvent.KEYCODE_ENTER) && (event.getAction() == KeyEvent.ACTION_DOWN))) {
                    saveKeyValue("coletor", coletor.getText().toString());
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity()).setMessage("Coletor salvo com sucesso!").setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            hideKeyboard(getActivity());
                        }
                    });
                    dialog.show();
                    return true;
                } else {
                    return false;
                }
            }
        });

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("coletor", Context.MODE_PRIVATE);


        this.email.setText(sharedPreferences.getString("email", "crhisnoriega@gmail.com"));
        this.coletor.setText(sharedPreferences.getString("coletor", "Coletor 1"));


        return root;
    }

    private void saveKeyValue(String key, String value) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("coletor", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private void askToClean() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity()).setMessage("Deseja limpar os dados coletados?").setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        database.productDao().nukeTable();
                        return null;
                    }
                };
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog.show();
    }

    private void writeToFile(List<Product> products) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy");
            String pathToMyAttachedFile = coletor.getText().toString() + "_" + sdf.format(Calendar.getInstance().getTime()) + ".csv";
            File root = Environment.getExternalStorageDirectory();
            File approot = new File(root, "coletor");
            if (!approot.exists()) {
                approot.mkdirs();
            }
            File file = new File(approot, pathToMyAttachedFile);
            Log.i("crhisn", "write to:" + file.getAbsolutePath());
            {

                FileOutputStream fos = new FileOutputStream(file);
                fos.write("EAN;Quantidade\n".getBytes());
                for (Product product : products) {
                    fos.write((product.getId() + ";" + product.getQuantidade() + "\n").getBytes());
                }
                fos.close();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
