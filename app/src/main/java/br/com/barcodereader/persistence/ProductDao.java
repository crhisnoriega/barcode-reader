package br.com.barcodereader.persistence;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ProductDao {
    @Query("SELECT * FROM product")
    List<Product> getAll();


    @Query("SELECT * FROM product WHERE id = :codebar")
    Product findByCodebar(String codebar);

    @Query("SELECT * FROM product WHERE id like :query")
    List<Product> findAnyCodebar(String query);

    @Insert
    void insertAll(Product... users);

    @Delete
    void delete(Product user);

    @Update
    void update(Product product);

    @Query("DELETE FROM product")
    public void nukeTable();
}
